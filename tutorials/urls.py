from django.conf.urls import url
from rest_framework.authtoken.views import obtain_auth_token

from tutorials import views

urlpatterns = [

    url(r'^api/tutorials$', views.TutorialList.as_view()),
    url(r'^api/tutorials/(?P<pk>[0-9]+)$', views.TutorialDetail.as_view()),

    #url(r'^api/tutorials$', views.tutorial_list),
    #url(r'^api/tutorials/(?P<pk>[0-9]+)$', views.tutorial_detail),
    #url(r'^api/tutorials/published$', views.tutorial_list_published),
    url(r'^api/authenticate/', obtain_auth_token),
]
